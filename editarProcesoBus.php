<?php 
	//print_r($_POST);
	if (!isset($_POST['oculto'])) {
		header('Location: Buses.php');
	}

	include 'model/conexion.php';
	$id = $_POST['id'];
	$placa2 = $_POST['txtplaca'];
	$modelo2 = $_POST['txt2modelo'];
	$color2 = $_POST['txt2color'];
	$capacidad2 = $_POST['txt2capacidad'];

	$sentencia = $bd->prepare("UPDATE bus SET placa = ?, modelo = ?, color = ?, capacidad = ? 
		WHERE id_bus = ?;");
	$resultado = $sentencia->execute([$placa2,$modelo2,$color2,$capacidad2,$id]);

	if ($resultado === TRUE) {
		header('Location: Buses.php');
	}else{
		echo "Error";
	}
?>