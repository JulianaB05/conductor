<?php  
	session_start();
	if (!isset($_SESSION['nombre'])) {
		header('Location: login.php');
	}elseif(isset($_SESSION['nombre'])){
		include 'model/conexion.php';
		$sentencia = $bd->query("SELECT * FROM conductor;");
		$conductores = $sentencia->fetchAll(PDO::FETCH_OBJ);
		//print_r($alumnos);
	}else{
		echo "Error en el sistema";
	}


	
?>

<!DOCTYPE html>
<html lang="es">
<head>
	<title>Inicio</title>
	<meta charset="utf-8">
<link rel="stylesheet" href="css/estilo.css">
<link rel="stylesheet" href="css/estilos2.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link href="css/miestilo.css" rel="stylesheet">
<link href="css/estilos.css" rel="stylesheet">
</head>

<body>
<?php 
include 'header.php';
 ?>

<h2>Conductores Activos</h2>
<hr>
<table class="table table-bordered">
  
  <thead>

    <tr>
      <th>Identificacion</th>
      <th>Nombre</th>
      <th>Apellidos</th>
      <th>Telefono</th>
      <th>Email</th>
      <th><a href="RegistrarConductor.php" class="btn__update">Agregar</a></th>

    </tr>

  </thead>
<tbody>
	
			<?php 
				foreach ($conductores as $dato) {
			?>
					<tr>
						<td><?php echo $dato->identificacion; ?></td>
						<td><?php echo $dato->nombre; ?></td>
						<td><?php echo $dato->apellidos; ?></td>
						<td><?php echo $dato->telefono; ?></td>
						<td><?php echo $dato->email; ?></td>
						<td><a href="editarconductor.php?identificacion=<?php echo $dato->identificacion; ?>" class="btn__update">Editar</a></td>
						<td><a href="eliminar.php?identificacion=<?php echo $dato->identificacion; ?>" class="btn__delete">Eliminar</a></td>
					</tr>
					<?php
				}
			?>
</tbody>		
		</table>
	
<!--inicio footer -->

<?php 
include 'footer.php';
 ?>

<!-- fin footer -- >
</div>

 
</body>
</html>