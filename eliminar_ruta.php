<?php  
	if (!isset($_GET['id_ruta'])) {
		exit();
	}

	$codigo = $_GET['id_ruta'];
	include 'model/conexion.php';
	$sentencia = $bd->prepare("DELETE FROM ruta WHERE id_ruta = ?;");
	$resultado = $sentencia->execute([$codigo]);

	if ($resultado === TRUE) {
		header('Location: rutas.php');
	}else{
		echo "Error";
	}

?>