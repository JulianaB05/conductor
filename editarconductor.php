<?php 
include 'clases.php'
 ?>
<?php  
	session_start();
	if (!isset($_SESSION['nombre'])) {
		header('Location: login.php');
	}elseif(isset($_SESSION['nombre'])){
		include 'model/conexion.php';
		$id = $_GET['identificacion'];
		$sentencia = $bd->prepare("SELECT * FROM conductor WHERE identificacion = ?;");
		$sentencia->execute([$id]);
		$conductor = $sentencia->fetch(PDO::FETCH_OBJ);
	}else{
		echo "Error en el sistema";
	}


	
?>
<!DOCTYPE html>
<html>
<head>
	<title>Editar Conductor</title>
	<meta charset="utf-8">
	

</head>
<body>
	<div class="container">

<header>
	<img src="img/conductor.jpg" style="width: 100%; height: 350px;" alt="">
</header>

	
		<h3>Editar Conductor:</h3>
		<form method="POST" action="editarProceso.php">
			<table class="table table-striped">
				<tr>
					<td >Nombre </td>
					<td><input size="100" type="text" name="txt2Nombre" value="<?php echo $conductor->nombre; ?>"></td>
				</tr>
				<tr>
					<td>Apellidos: </td>
					<td><input size="100" type="text" name="txt2Apellidos" value="<?php echo $conductor->apellidos; ?>"></td>
				</tr>
				<tr>
					<td>Telefono: </td>
					<td><input size="100" type="text" name="txt2Telefono" value="<?php echo $conductor->telefono; ?>"></td>
				</tr>
				<tr>
					<td>Email: </td>
					<td><input type="text" name="txt2Email" value="<?php echo $conductor->email; ?>"></td>
				</tr>

				<tr>
					<input type="hidden" name="oculto">
					<input type="hidden" name="identificacion" value="<?php echo $conductor->identificacion; ?>">
					<td colspan="2"><input type="submit" value="ACTUALIZAR CONDUCTOR" class="btn btn-info"></td>
					<td colspan="2"><a href="index.php" style="display: inline-block;font-size: 14px;background: #8a0505;color: #fff;border-radius: 5px;padding: 5px 10px;">Volver</a></td>
				</tr>
			</table>
		</form>
	
<?php 
include 'footer.php';
 ?>
	
</div>


<?php 
include 'script.php'
 ?>	
 
</body>
</html>