<?php  
	session_start();
	if (!isset($_SESSION['nombre'])) {
		header('Location: login.php');
	}elseif(isset($_SESSION['nombre'])){
		include 'model/conexion.php';
		$sentencia=$bd->query("Select c.id_contrato,c.fecha_inicio,c.fecha_fin,c.valor,co.nombre,co.apellidos,co.identificacion from contrato as c inner join conductor as co on c.id_conductor=co.id_conductor;");
$contrato=$sentencia->fetchAll(PDO::FETCH_OBJ);
	}else{
		echo "Error en el sistema";
	}


	
?>
<!DOCTYPE html>
<html lang="es">
<head>
	

<header>
	<meta charset="utf-8">
<link rel="stylesheet" href="css/estilo.css">
<link rel="stylesheet" href="css/estilos2.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link href="css/miestilo.css" rel="stylesheet">
<link href="css/estilos.css" rel="stylesheet">
</header>

<body>
<?php 
include 'header.php';
 ?>
<h2>Contratos Activos</h2>
<hr>
<table class="table table-bordered">
  
  <thead>

    <tr>
      <th>Fecha Inicial</th>
      <th>Fecha Terminacion</th>
      <th>Valor</th>
      <th>Conductor</th>
      <th>Identificacion</th>
      
      <th><a href="Registrarcontrato.php" class="btn__update">Agregar</a></th>

    </tr>

  </thead>
<tbody>
	
			<?php 
				foreach ($contrato as $dato) {
			?>
					<tr>
						<td><?php echo $dato->fecha_inicio; ?></td>
						<td><?php echo $dato->fecha_fin; ?></td>
						<td><?php echo $dato->valor; ?></td>
						<td><?php echo $dato->nombre; ?> <?php echo $dato->apellidos; ?></td>
						<td><?php echo $dato->identificacion; ?></td>
						

						<td><a href="editarcontrato.php?id_contrato=<?php echo $dato->id_contrato; ?>" class="btn__update">Editar</a></td>
						<td><a href="eliminarcontrato.php?id_contrato=<?php echo $dato->id_contrato; ?>" class="btn__delete">Eliminar</a></td>
					</tr>
					<?php
				}
			?>
</tbody>		
		</table>
	
<!--inicio footer -->

<?php 
include 'footer.php';
 ?>

<!-- fin footer -- >
</div>

 
</body>
</html>