<div class="container">
<?php  
	if (!isset($_POST['oculto'])) {
		exit();
	}

	include 'model/conexion.php';
	$placa = $_POST['txtPlaca'];
	$modelo = $_POST['txtModelo'];
	$color = $_POST['txtColor'];
	$capacidad = $_POST['txtCapacidad'];
	$conductor = $_POST['txtConductor'];
	$ruta = $_POST['txtRuta'];

	$sentencia = $bd->prepare("INSERT INTO bus (placa,modelo,color,capacidad,id_conductor,id_ruta) VALUES (?,?,?,?,?,?);");
	$resultado = $sentencia->execute([$placa,$modelo,$color,$capacidad,$conductor,$ruta]);

	if ($resultado === TRUE) {
		//echo "Insertado correctamente";
		header('Location: Buses.php');
	}else{
		echo "Error";
	}
?>
</div>