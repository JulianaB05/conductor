<?php  
	if (!isset($_GET['identificacion'])) {
		exit();
	}

	$codigo = $_GET['identificacion'];
	include 'model/conexion.php';
	$sentencia = $bd->prepare("DELETE FROM conductor WHERE identificacion = ?;");
	$resultado = $sentencia->execute([$codigo]);

	if ($resultado === TRUE) {
		header('Location: index.php');
	}else{
		echo "Error";
	}

?>