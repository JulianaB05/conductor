
<?php  
	session_start();
	if (!isset($_SESSION['nombre'])) {
		header('Location: login.php');
	}elseif(isset($_SESSION['nombre'])){
		include 'model/conexion.php';
		$sentencia=$bd->query("Select r.id_ruta,r.nombre_ruta,r.origen,r.destino,b.capacidad,c.nombre,c.apellidos,b.placa from ruta as r inner join bus as b on r.id_bus=b.id_bus inner join conductor as c on b.id_conductor=c.id_conductor;");
		$ruta=$sentencia->fetchAll(PDO::FETCH_OBJ);
		//print_r($alumnos);
	}else{
		echo "Error en el sistema";
	}


	
?>
<!DOCTYPE html>
<html lang="es">
<head>
	

<header>
	<meta charset="utf-8">
<link rel="stylesheet" href="css/estilo.css">
<link rel="stylesheet" href="css/estilos2.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link href="css/miestilo.css" rel="stylesheet">
<link href="css/estilos.css" rel="stylesheet">
</header>

<body>
<?php 
include 'header.php';
 ?>
<h2>Rutas Activos</h2>
<hr>
<table class="table table-bordered">
  
  <thead>

    <tr>
      <th>Nombre ruta</th>
      <th>Origen</th>
      <th>Destino</th>
      <th>Placa</th>
      <th>Conductor</th>
      <th><a href="RegistrarRuta.php" class="btn__update">Agregar</a></th>

    </tr>

  </thead>
<tbody>
	
			<?php 
				foreach ($ruta as $dato) {
			?>
					<tr>
						<td><?php echo $dato->nombre_ruta; ?></td>
						<td><?php echo $dato->origen; ?></td>
						<td><?php echo $dato->destino; ?></td>
						<td><?php echo $dato->placa; ?></td>
						<td><?php echo $dato->nombre; ?> <?php echo $dato->apellidos; ?></td>
						<td><a href="editar_ruta.php?id_ruta=<?php echo $dato->id_ruta; ?>" class="btn__update">Editar</a></td>
						<td><a href="eliminar_ruta.php?id_ruta=<?php echo $dato->id_ruta; ?>" class="btn__delete">Eliminar</a></td>
					</tr>
					<?php
				}
			?>
</tbody>		
		</table>
	
<!--inicio footer -->

<?php 
include 'footer.php';
 ?>

<!-- fin footer -- >
</div>

 
</body>
</html>