<?php 
	//print_r($_POST);
	if (!isset($_POST['oculto'])) {
		header('Location: rutas.php');
	}

	include 'model/conexion.php';
	$id = $_POST['id_ruta'];
	$nombre_ruta = $_POST['txtnombre_ruta'];
	$origen = $_POST['txt2origen'];
	$destino = $_POST['txt2destino'];

	$sentencia = $bd->prepare("UPDATE ruta SET nombre_ruta = ?, origen = ?, destino = ?
		WHERE id_ruta = ?;");
	$resultado = $sentencia->execute([$nombre_ruta,$origen,$destino,$id]);

	if ($resultado === TRUE) {
		header('Location: rutas.php');
	}else{
		echo "Error";
	}
?>