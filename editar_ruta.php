<?php 
include 'clases.php'
 ?>
<?php  
	session_start();
	if (!isset($_SESSION['nombre'])) {
		header('Location: login.php');
	}elseif(isset($_SESSION['nombre'])){
		include 'model/conexion.php';
		$id = $_GET['id_ruta'];
		$sentencia = $bd->prepare("select r.id_ruta,r.nombre_ruta,r.origen,r.destino,b.capacidad,c.nombre,c.apellidos,b.placa from ruta as r inner join bus as b on r.id_bus=b.id_bus inner join conductor as c on b.id_conductor=c.id_conductor WHERE r.id_ruta = ?");
		$sentencia->execute([$id]);
		$ruta = $sentencia->fetch(PDO::FETCH_OBJ);
	}else{
		echo "Error en el sistema";
	}


	
?>

<!DOCTYPE html>
<html>
<head>
	<title>Editar Ruta</title>
	<meta charset="utf-8">
	

</head>
<body>
	<div class="container">

<header>
	<img src="img/conductor.jpg" style="width: 100%; height: 350px;" alt="">
</header>

	
		<h3>Editar Bus:</h3>
		<form method="POST" action="editarProcesoruta.php">
			<table class="table table-striped">
				<tr>
					<td >Nombre Ruta </td>
					<td><input size="100" type="text" name="txtnombre_ruta" value="<?php echo $ruta->nombre_ruta; ?>"></td>
				</tr>
				<tr>
					<td>Origen </td>
					<td><input size="100" type="text" name="txt2origen" value="<?php echo $ruta->origen; ?>"></td>
				</tr>
				<tr>
					<td>Destino </td>
					<td><input size="100" type="text" name="txt2destino" value="<?php echo $ruta->destino; ?>"></td>
				</tr>
				<tr>
					<td>Conductor </td>
					<td><?php echo $ruta->nombre; ?> <?php echo $ruta->apellidos; ?></td>
				</tr>
				<tr>
					<td>Bus </td>
					<td><?php echo $ruta->placa; ?> </td>
				</tr>
				<tr>
					<input type="hidden" name="oculto">
					<input type="hidden" name="id_ruta" value="<?php echo $ruta->id_ruta; ?>">
					<td colspan="2"><input type="submit" value="ACTUALIZAR BUS" class="btn btn-info"></td>
					<td colspan="2"><a href="rutas.php" style="display: inline-block;font-size: 14px;background: #8a0505;color: #fff;border-radius: 5px;padding: 5px 10px;">Volver</a></td>
				</tr>
				
			</table>
		</form>
	
<?php 
include 'footer.php';
 ?>
	
</div>


<?php 
include 'script.php'
 ?>	
 
</body>
</html>