<?php  
	if (!isset($_GET['id_contrato'])) {
		exit();
	}

	$codigo = $_GET['id_contrato'];
	include 'model/conexion.php';
	$sentencia = $bd->prepare("DELETE FROM contrato WHERE id_contrato = ?;");
	$resultado = $sentencia->execute([$codigo]);

	if ($resultado === TRUE) {
		header('Location: contratos.php');
	}else{
		echo "Error";
	}

?>