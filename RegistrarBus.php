
<?php  
	session_start();
	if (!isset($_SESSION['nombre'])) {
		header('Location: login.php');
	}elseif(isset($_SESSION['nombre'])){
		include 'model/conexion.php';
		echo "Error en el sistema";
	}


	
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Registro Buses</title>

<link rel="stylesheet" href="css/estilo.css">
<link rel="stylesheet" href="css/estilos2.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/style_nav.css" rel="stylesheet">
<link href="css/font-awesome.css" rel="stylesheet"/>

</head>

<body>
<div class="container">
</div>
<div class="container">
<img src="img/conductor2.jpg" style="width: 100%; height: 300px;" alt="">
</div>

<div class="container">
<center>		
<h3>Registrar Bus:</h3>
		<form method="POST" action="insertarBus.php">
			<table class="table table-bordered">
				<tr>
					<td>Placa: </td>
					<td><input type="text" name="txtPlaca"></td>
				</tr>
				<tr>
					<td>Modelo: </td>
					<td><input type="text" name="txtModelo"></td>
				</tr>
				<tr>
					<td>Color: </td>
					<td><input type="text" name="txtColor"></td>
				</tr>
				<tr>
					<td>Capacidad: </td>
					<td><input type="number" name="txtCapacidad"></td>
				</tr>
				<tr>
					<td>Conductor: </td>
					<td>
					<select name="txtConductor">
			        <option value="0">Seleccione Conductor</option>
			        <?php
			        	include 'model/conexion.php';
			          $query = $mysqli -> query ("SELECT * FROM conductor");
			          while ($valores = mysqli_fetch_array($query)) {
			            echo '<option value="'.$valores[id_conductor].'">'.$valores[nombre].'</option>';
			          }
			        ?>
			      </select></td>
				</tr>
				<tr>
					<td>Ruta: </td>
					<td><select name="txtRuta">
			        <option value="0">Seleccione Conductor</option>
			        <?php
			        	include 'model/conexion.php';
			          $query = $mysqli -> query ("SELECT * FROM ruta");
			          while ($valores = mysqli_fetch_array($query)) {
			            echo '<option value="'.$valores[id_ruta].'">'.$valores[nombre_ruta].'</option>';
			          }
			        ?>
			      </select></td>
				</tr>
				<input type="hidden" name="oculto" value="1">
				<tr>
					<td><input type="reset" name="" class="btn__update"></td>
					<td><input type="submit" value="¡REGISTRARSE!" class="btn__update"></td>
				</tr>
			</table>
		</form>
		<!-- fin insert-->

	</center>
	</div>

<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="js/jquery.min.js"></script>
<script src="js/jquery-1.10.2.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap.js"></script>

</body>
</html>