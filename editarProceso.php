<?php 
	//print_r($_POST);
	if (!isset($_POST['oculto'])) {
		header('Location: index.php');
	}

	include 'model/conexion.php';
	$identificacion = $_POST['identificacion'];
	$nombre2 = $_POST['txt2Nombre'];
	$apellidos2 = $_POST['txt2Apellidos'];
	$telefono2 = $_POST['txt2Telefono'];
	$email2 = $_POST['txt2Email'];

	$sentencia = $bd->prepare("UPDATE conductor SET nombre = ?, apellidos = ?, telefono = ?, 
												email = ? WHERE identificacion = ?;");
	$resultado = $sentencia->execute([$nombre2,$apellidos2,$telefono2,$email2,$identificacion]);

	if ($resultado === TRUE) {
		header('Location: index.php');
	}else{
		echo "Error";
	}
?>