
<?php  
	session_start();
	if (!isset($_SESSION['nombre'])) {
		header('Location: login.php');
	}elseif(isset($_SESSION['nombre'])){
		include 'model/conexion.php';
		$sentencia=$bd->query("Select b.placa,b.modelo,b.color,b.capacidad,c.nombre,c.apellidos,r.nombre_ruta from bus as b inner join conductor as c on b.id_conductor=c.id_conductor inner join ruta as r on b.id_ruta=r.id_ruta;");
		$bus=$sentencia->fetchAll(PDO::FETCH_OBJ);
		//print_r($alumnos);
	}else{
		echo "Error en el sistema";
	}


	
?>
<!DOCTYPE html>
<html lang="es">
<head>
	

<header>
	<meta charset="utf-8">
<link rel="stylesheet" href="css/estilo.css">
<link rel="stylesheet" href="css/estilos2.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link href="css/miestilo.css" rel="stylesheet">
<link href="css/estilos.css" rel="stylesheet">
</header>

<body>
<?php 
include 'header.php';
 ?>
<h2>Buses Activos</h2>
<hr>
<table class="table table-bordered">
  
  <thead>

    <tr>
      <th>Placa</th>
      <th>Color</th>
      <th>Modelo</th>
      <th>Capacidad</th>
      <th>Conductor</th>
      <th>Nombre Ruta</th>
      <th><a href="RegistrarBus.php" class="btn__update">Agregar</a></th>

    </tr>

  </thead>
<tbody>
	
			<?php 
				foreach ($bus as $dato) {
			?>
					<tr>
						<td><?php echo $dato->placa; ?></td>
						<td><?php echo $dato->color; ?></td>
						<td><?php echo $dato->modelo; ?></td>
						<td><?php echo $dato->capacidad; ?> Personas</td>
						<td><?php echo $dato->nombre, $dato->apellidos; ?></td>
						<td><?php echo $dato->nombre_ruta; ?></td>
						<td><a href="editarbus.php?placa=<?php echo $dato->placa; ?>" class="btn__update">Editar</a></td>
						<td><a href="eliminarBus.php?placa=<?php echo $dato->placa; ?>" class="btn__delete">Eliminar</a></td>
					</tr>
					<?php
				}
			?>
</tbody>		
		</table>
	
<!--inicio footer -->

<?php 
include 'footer.php';
 ?>

<!-- fin footer -- >
</div>

 
</body>
</html>