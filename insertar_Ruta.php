<div class="container">
<?php  
	if (!isset($_POST['oculto'])) {
		exit();
	}

	include 'model/conexion.php';
	$nombre_ruta = $_POST['txtRuta'];
	$origen = $_POST['txtOrigen'];
	$destino = $_POST['txtDestino'];
	$bus = $_POST['txtBus'];


	$sentencia = $bd->prepare("INSERT INTO ruta (nombre_ruta,origen,destino,id_bus) VALUES (?,?,?,?);");
	$resultado = $sentencia->execute([$nombre_ruta,$origen,$destino,$bus]);

	if ($resultado === TRUE) {
		//echo "Insertado correctamente";
		header('Location: rutas.php');
	}else{
		echo "Error";
	}
?>
</div>