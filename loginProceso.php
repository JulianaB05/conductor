<?php 
	session_start();
	include_once 'model/conexion.php';
	$identificacion = $_POST['txtidentificacion'];
	$contrasena = $_POST['txtPass'];
	$sentencia = $bd->prepare('select * from conductor where 
								identificacion = ? and password = ?;');
	$sentencia->execute([$identificacion, $contrasena]);
	$datos = $sentencia->fetch(PDO::FETCH_OBJ);
	//print_r($datos);

	if ($datos === FALSE) {
		header('Location: login.php');
	}elseif($sentencia->rowCount() == 1){
		$_SESSION['nombre'] = $datos->identificacion;
		header('Location: index.php');
	}
?>