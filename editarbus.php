<?php 
include 'clases.php'
 ?>
<?php  
	session_start();
	if (!isset($_SESSION['nombre'])) {
		header('Location: login.php');
	}elseif(isset($_SESSION['nombre'])){
		include 'model/conexion.php';
		$id = $_GET['placa'];
		$sentencia = $bd->prepare("Select b.id_bus,b.placa,b.modelo,b.color,b.capacidad,c.nombre,c.apellidos,r.nombre_ruta from bus as b inner join conductor as c on b.id_conductor=c.id_conductor inner join ruta as r on b.id_ruta=r.id_ruta WHERE PLACA = ?");
		$sentencia->execute([$id]);
		$bus = $sentencia->fetch(PDO::FETCH_OBJ);
	}else{
		echo "Error en el sistema";
	}


	
?>

<!DOCTYPE html>
<html>
<head>
	<title>Editar Bus</title>
	<meta charset="utf-8">
	

</head>
<body>
	<div class="container">

<header>
	<img src="img/conductor.jpg" style="width: 100%; height: 350px;" alt="">
</header>

	
		<h3>Editar Bus:</h3>
		<form method="POST" action="editarProcesoBus.php">
			<table class="table table-striped">
				<tr>
					<td >Placa </td>
					<td><input size="100" type="text" name="txtplaca" value="<?php echo $bus->placa; ?>"></td>
				</tr>
				<tr>
					<td>Modelo: </td>
					<td><input size="100" type="text" name="txt2modelo" value="<?php echo $bus->modelo; ?>"></td>
				</tr>
				<tr>
					<td>Color: </td>
					<td><input size="100" type="text" name="txt2color" value="<?php echo $bus->color; ?>"></td>
				</tr>
				<tr>
					<td>Capacidad: </td>
					<td><input size="100" type="number" name="txt2capacidad" value="<?php echo $bus->capacidad; ?>"></td>
				</tr>
				<tr>
					<td>Conductor: </td>
					<td><?php echo $bus->nombre; ?> <?php echo $bus->apellidos; ?></td>
				</tr>
				<tr>
					<td>Ruta: </td>
					<td><?php echo $bus->nombre_ruta; ?> </td>
				</tr>
				<tr>
					<input type="hidden" name="oculto">
					<input type="hidden" name="id" value="<?php echo $bus->id_bus; ?>">
					<td colspan="2"><input type="submit" value="ACTUALIZAR BUS" class="btn btn-info"></td>
					<td colspan="2"><a href="Buses.php" style="display: inline-block;font-size: 14px;background: #8a0505;color: #fff;border-radius: 5px;padding: 5px 10px;">Volver</a></td>
				</tr>
				
			</table>
		</form>
	
<?php 
include 'footer.php';
 ?>
	
</div>


<?php 
include 'script.php'
 ?>	
 
</body>
</html>