<div class="container">
<?php  
	if (!isset($_POST['oculto'])) {
		exit();
	}

	include 'model/conexion.php';
	$fechaini = $_POST['txtIfechaini'];
	$fechafin = $_POST['txtfechafin'];
	$valor = $_POST['txtvalor'];
	$conductor = $_POST['txtConductor'];

	$sentencia = $bd->prepare("INSERT INTO contrato(fecha_inicio,fecha_fin,valor,id_conductor) VALUES (?,?,?,?);");
	$resultado = $sentencia->execute([$fechaini,$fechafin,$valor,$conductor]);

	if ($resultado === TRUE) {
		//echo "Insertado correctamente";
		header('Location: contratos.php');
	}else{
		echo "Error";
	}
?>
</div>