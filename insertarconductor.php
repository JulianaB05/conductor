<div class="container">
<?php  
	if (!isset($_POST['oculto'])) {
		exit();
	}

	include 'model/conexion.php';
	$identificacion = $_POST['txtIdentificacion'];
	$nombre = $_POST['txtNombre'];
	$apellidos = $_POST['txtApellidos'];
	$telefono = $_POST['txtTelefono'];
	$email = $_POST['txtEmail'];
	$contraseña = $_POST['txtContraseña'];

	$sentencia = $bd->prepare("INSERT INTO conductor(identificacion,nombre,apellidos,telefono,email,password) VALUES (?,?,?,?,?,?);");
	$resultado = $sentencia->execute([$identificacion,$nombre,$apellidos,$telefono,$email,$contraseña]);

	if ($resultado === TRUE) {
		//echo "Insertado correctamente";
		header('Location: login.php');
	}else{
		echo "Error";
	}
?>
</div>