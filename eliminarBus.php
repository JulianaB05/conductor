<?php  
	if (!isset($_GET['placa'])) {
		exit();
	}

	$codigo = $_GET['placa'];
	include 'model/conexion.php';
	$sentencia = $bd->prepare("DELETE FROM bus WHERE placa = ?;");
	$resultado = $sentencia->execute([$codigo]);

	if ($resultado === TRUE) {
		header('Location: Buses.php');
	}else{
		echo "Error";
	}

?>