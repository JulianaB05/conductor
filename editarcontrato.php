<?php 
include 'clases.php'
 ?>

	<?php  
	session_start();
	if (!isset($_SESSION['nombre'])) {
		header('Location: login.php');
	}elseif(isset($_SESSION['nombre'])){
		include 'model/conexion.php';
		$id = $_GET['id_contrato'];
		$sentencia = $bd->prepare("Select c.id_contrato,c.fecha_inicio,c.fecha_fin,c.valor,co.nombre,co.apellidos,co.identificacion from contrato as c inner join conductor as co on c.id_conductor=co.id_conductor WHERE id_contrato = ?");
		$sentencia->execute([$id]);
		$contrato = $sentencia->fetch(PDO::FETCH_OBJ);
	}else{
		echo "Error en el sistema";
	}


	
?>

<!DOCTYPE html>
<html>
<head>
	<title>Editar Contrato</title>
	<meta charset="utf-8">
	

</head>
<body>
	<div class="container">

<header>
	<img src="img/conductor.jpg" style="width: 100%; height: 350px;" alt="">
</header>

	
		<h3>Editar Contrato:</h3>
		<form method="POST" action="editarProcesocontrato.php">
			<table class="table table-striped">
				<tr>
					<td >Fecha Inicial </td>
					<td><input size="100" type="date" name="txtfechaini" value="<?php echo $contrato->fecha_inicio; ?>"></td>
				</tr>
				<tr>
					<td>Fecha Finalizacion </td>
					<td><input size="100" type="date" name="txt2fechafin" value="<?php echo $contrato->fecha_fin; ?>"></td>
				</tr>
				<tr>
					<td>Valor</td>
					<td><input size="100" type="number" name="txt2valor" value="<?php echo $contrato->valor; ?>"></td>
				</tr>
				<tr>
					<td>Conductor: </td>
					<td><?php echo $contrato->nombre; ?> <?php echo $contrato->apellidos; ?></td>
				</tr>
				<tr>
					<input type="hidden" name="oculto">
					<input type="hidden" name="id" value="<?php echo $contrato->id_contrato; ?>">
					<td colspan="2"><input type="submit" value="ACTUALIZAR CONTRATO" class="btn btn-info"></td>
					<td colspan="2"><a href="contratos.php" style="display: inline-block;font-size: 14px;background: #8a0505;color: #fff;border-radius: 5px;padding: 5px 10px;">Volver</a></td>
				</tr>
				
			</table>
		</form>
	
<?php 
include 'footer.php';
 ?>
	
</div>


<?php 
include 'script.php'
 ?>	
 
</body>
</html>