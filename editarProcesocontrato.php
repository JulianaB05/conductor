<?php 
	//print_r($_POST);
	if (!isset($_POST['oculto'])) {
		header('Location: contratos.php');
	}

	include 'model/conexion.php';
	$id = $_POST['id'];
	$fechaini = $_POST['txtfechaini'];
	$fechafin = $_POST['txt2fechafin'];
	$valor = $_POST['txt2valor'];

	$sentencia = $bd->prepare("UPDATE contrato SET fecha_inicio = ?, fecha_fin = ?, valor = ?
		WHERE id_contrato = ?;");
	$resultado = $sentencia->execute([$fechaini,$fechafin,$valor,$id]);

	if ($resultado === TRUE) {
		header('Location: contratos.php');
	}else{
		echo "Error";
	}
?>